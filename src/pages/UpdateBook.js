import BookListEdit from '../components/books/BookListEdit'
const UpdateBook=(props)=>{
    return <div>
    {props.getBooks.length===0 && <p>No Books Found</p>}
    {props.getBooks.length>0 && <BookListEdit books={props.getBooks} setBooks={props.setBooks} />}
    </div>
}
export default UpdateBook;

