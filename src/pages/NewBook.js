import { useHistory } from "react-router-dom";
import BookForm from "../components/books/BookForm";
const NewBook=(props)=>{
    const history=useHistory();
    const addBookHandler=(bookdetails)=>{
       // props.addBook(bookdetails);
        props.setBooks([...props.getBooks,bookdetails]);
        history.push('/books')
    }
    return <BookForm onAddBook={addBookHandler} />

}
export default NewBook;