
import DeleteBook from '../components/books/DeleteBook';
import { useHistory } from 'react-router-dom';
const DeleteBookPage=(props)=>{
    const history=useHistory();
    
    const onDeleteHandler=(bookname)=>{
        const deletebook=props.getBooks.filter((book)=>
        {
            return book.name===bookname.trim();
        })
        
        
        if(deletebook.length===0)
        {
            alert("books are not present");
            history.push('/books')
        }
        else if(!deletebook[0].customer)
       {    
           
           const afterDelete=props.getBooks.filter((book)=>book.name!==bookname)
           console.log(afterDelete);
           props.setBooks(afterDelete);
           history.push('/books')

       }
       else{
        console.log("check2")
        alert("customers are present")
        history.push('/books')
       }

    }

    return <DeleteBook onDelete={onDeleteHandler}  />

}
export default DeleteBookPage;
//getBooks={getBooks} setBooks={getBooks}