import { Fragment } from 'react';
import {useParams} from 'react-router-dom'
import HighlightedBook from '../components/books/HighlightedQuote';

const BookDetails=(props)=>{

    const param=useParams();
   
    const book=props.getBooks.find((b)=>{
        
        return b.id=== parseInt(param.bookId)}
        )
    if(!book){
        return <p>No Book Found</p>
    }
    return <Fragment>
    <HighlightedBook name={book.name} author={book.author} customer={book.customer} edition={book.edition} price={book.price}/>
  
    </Fragment>

}
export default BookDetails;