import BookList from "../components/books/BookList";

const AllBooks=(props)=>{
   
    return <BookList books={props.getBooks} />
}
export default AllBooks;