import { Fragment } from 'react';
import {useParams,useHistory} from 'react-router-dom'
import EditForm from '../components/books/EditForm'

const EditBookDetails=(props)=>{
    const history=useHistory();
    const param=useParams();
    const onSaveHandler=(editeddetails)=>{
    const newArray=props.getBooks.map((book)=>{
            if(book.id===editeddetails.id)
            {
                return {...book,name:editeddetails.name,customer:editeddetails.customer,author:editeddetails.author,price:editeddetails.price,
                edition:editeddetails.edition}
            }
            return book;
        })      
        props.setBooks(newArray)
        history.push('/books');
    }
    const book=props.getBooks.find((b)=>{
        return b.id=== parseInt(param.editId)}
        )
    if(!book){
        return <p>No Book Found</p>
    }
    return <Fragment>
        <EditForm name={book.name} author={book.author} id={book.id} customer={book.customer}  
        edition={book.edition} price={book.price} OnSaveHandler={onSaveHandler}/>
    </Fragment>
}
export default EditBookDetails;