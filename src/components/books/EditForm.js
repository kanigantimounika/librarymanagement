import { useState } from 'react';
import Card from '../UI/Card';
import classes from './BookForm.module.css';

const EditForm=(props)=>{

    const [name,setName]=useState(props.name);
    const [author,setAuthor]=useState(props.author);
    const [customer,setCustomer]=useState(props.customer);
    const [price,setPrice]=useState(props.price);
    const [edition,setEdition]=useState(props.edition);
   
    const onSubmitHandler=(e)=>{
        e.preventDefault();
        const book={
            id:props.id,name:name,author:author,customer:customer,price:price,edition:edition
        }
       
        props.OnSaveHandler(book)
    }

    const onNameChange=(e)=>{
        setName(e.target.value)
    }
    const onAuthorChange=(e)=>{
        setAuthor(e.target.value)
    }
    const onCustomerChange=(e)=>{
        setCustomer(e.target.value)
    }
    const onEditionChange=(e)=>{
      setEdition(e.target.value)
    }
    const onPriceChange=(e)=>{
      setPrice(e.target.value)
    }
    return <Card>
      <form className={classes.form}  onSubmit={onSubmitHandler}>
        
        <div className={classes.control}>
          <label htmlFor='name'>Name</label>
          <input id='name'  value={name} onChange={onNameChange} />
        </div>
        <div className={classes.control}>
          <label htmlFor='author'>Author</label>
          <input id='author'  value={author}  onChange={onAuthorChange}/>
        </div>
        <div className={classes.control}>
        <label htmlFor='price'>Price</label>
        <input id='price'value={price} onChange={onPriceChange}/>
      </div>
      <div className={classes.control}>
        <label htmlFor='edition'>Edition</label>
        <input id='edition'value={edition} onChange={onEditionChange}/>
      </div>
        <div className={classes.control}>
          <label htmlFor='customer'>Customer</label>
          <input id='customer'value={customer} onChange={onCustomerChange}/>
        </div>
       
        <div className={classes.actions}>
          <button className='btn'>Save</button>
        </div>
      </form>
    </Card>

}
export default EditForm;