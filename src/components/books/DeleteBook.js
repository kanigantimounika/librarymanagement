import { Fragment, useRef } from "react";
import classes from './BookForm.module.css';
import Card from '../UI/Card';


const DeleteBook=(props)=>{
    const nameInputRef = useRef();
   

    const onDeleteHandler=()=>{
       props.onDelete(nameInputRef.current.value)

    }
    return <Fragment>

            <Card>
            
              
                <div className={classes.control}>
                    <label htmlFor='customer'>Name</label>
                    <input id='customer'  ref={nameInputRef} />
                </div>
                <div className={classes.actions}>
                    <button className='btn' onClick={onDeleteHandler} >Delete Book</button>
                </div>
          
            </Card>
            </Fragment>

}
export default DeleteBook