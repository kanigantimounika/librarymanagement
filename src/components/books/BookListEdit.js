import { Fragment } from 'react';

import BookItemEdit from './BookItemEdit';
import classes from './BookList.module.css';

const BookListEdit = (props) => {
  
  return (
    <Fragment>
      <ul className={classes.list}>
        {props.books ?props.books.map((book) => (
          <BookItemEdit
            key={book.id}
            id={book.id}
            author={book.author}
            text={book.name}
            customer={book.customer}
            price={book.price}
            edition={book.edition}
          />
        )):<p>No Books</p>}
      </ul>
    </Fragment>
  );
};
export default BookListEdit