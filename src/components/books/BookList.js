import { Fragment } from 'react';

import BookItem from './BookItem';
import classes from './BookList.module.css';

const BookList = (props) => {
  
  return (
    <Fragment>
      <ul className={classes.list}>
        {props.books ?props.books.map((book) => (
          <BookItem
            key={book.id}
            id={book.id}
            author={book.author}
            text={book.name}
            edition={book.edition}
            price={book.price}
            customer={book.customer}

          />
        )):<p>No Books</p>}
      </ul>
    </Fragment>
  );
};

export default BookList;
