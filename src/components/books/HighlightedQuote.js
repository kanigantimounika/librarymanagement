import classes from './HighlightedBook.module.css';

const HighlightedBook = (props) => {
  return (
    <figure className={classes.quote}>
    <p> Name : {props.name}</p>
    <p>Author: {props.author}</p>
    <p>Customer: {props.customer}</p>
    <p>Edition: {props.edition}</p>
    <p>Price: {props.price}</p>
     
    </figure>
  );
};

export default HighlightedBook;
