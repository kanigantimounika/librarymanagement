import { useRef } from 'react';

import Card from '../UI/Card';
import LoadingSpinner from '../UI/LoadingSpinner';
import classes from './BookForm.module.css';

const BookForm = (props) => {
  const authorInputRef = useRef();
  const customerInputRef = useRef();
  const nameInputRef = useRef();
  const editionRef=useRef();
  const priceRef=useRef();

  function submitFormHandler(event) {
    event.preventDefault();

    const enteredAuthor = authorInputRef.current.value;
    const enteredCustomer = customerInputRef.current.value;
    const enteredName = nameInputRef.current.value;
    const enteredEdition=editionRef.current.value;
    const enteredPrice =priceRef.current.value;
    props.onAddBook({name: enteredName,author: enteredAuthor, customer: enteredCustomer,id:parseInt(Date.now()),price:enteredPrice,edition:enteredEdition});
  }

  return (
    <Card>
      <form className={classes.form} onSubmit={submitFormHandler}>
        {props.isLoading && (
          <div className={classes.loading}>
            <LoadingSpinner />
          </div>
        )}
        <div className={classes.control}>
          <label htmlFor='name'>Name</label>
          <input id='name'  ref={nameInputRef}></input>
        </div>
        <div className={classes.control}>
          <label htmlFor='author'>Author</label>
          <input type='text' id='author' ref={authorInputRef} ></input>
        </div>
        <div className={classes.control}>
          <label htmlFor='edition'>Edition</label>
          <input id='edition' ref={editionRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor='price'>Price</label>
          <input id='price' ref={priceRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor='customer'>Customer</label>
          <input id='customer' ref={customerInputRef} />
        </div>
        <div className={classes.actions}>
          <button className='btn'>Add Quote</button>
        </div>
      </form>
    </Card>
  );
};

export default BookForm;
