import classes from './MainNavigation.module.css';
import { NavLink } from 'react-router-dom';
const MainNavigation=(props)=>{
   
    return <header className={classes.header}>
        {!props.headerDisplay ?  <div className={classes.logo}>Login</div> :  <div className={classes.logo}>Books</div>}
        {props.headerDisplay && 
        <nav className={classes.nav}>
            <ul>
                <li><NavLink activeClassName={classes.active} to="/books">Books</NavLink></li>
                <li><NavLink activeClassName={classes.active} to="/new-book">Add Book</NavLink></li>
                <li><NavLink activeClassName={classes.active} to="/delete">Delete Book</NavLink></li>
                <li><NavLink activeClassName={classes.active} to="/edit">Update Book</NavLink></li>
                <li>
                     <span activeClassName={classes.active} onClick={props.onLogout}>Logout</span></li>
            </ul>
        
        </nav>}



    </header>

}
export default MainNavigation