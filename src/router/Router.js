import React,{Suspense} from 'react';
import {Route,Switch,Redirect} from 'react-router-dom'
import Layout from '../components/layout/Layout';
import { useState } from 'react';
import Login from '../components/login/Login';
import { withRouter } from 'react-router-dom';
import LoadingSpinner from '../components/UI/LoadingSpinner';

const Router=(props) =>{
  const NewBook=React.lazy(()=>import('../pages/NewBook'))
  const AllBooks=React.lazy(()=>import('../pages/AllBooks'));
  const BookDetails=React.lazy(()=>import('../pages/BookDetails'))
  const PageNotFound=React.lazy(()=>import('../pages/PageNotFound'))
  const DeleteBook=React.lazy(()=>import('../pages/DeleteBook'))
  const UpdateBook=React.lazy(()=>import('../pages/UpdateBook'));
  const EditBookDetails=React.lazy(()=>import('../pages/EditBookDetails'));
  const [getBooks,setBooks]=useState([]);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isHome,setHome]=useState(false);
  const onLoginHandler=()=>{
    setIsAuthenticated(true)
    setHome(true);
    props.history.push("/books")
  }
  const onLogoutHandler=()=>{
    setHome(false);
    setIsAuthenticated(false);
    props.history.push("*");
  }
  return (
      <Layout onLogout={onLogoutHandler} headerDisplay={isHome}>
      <Suspense fallback={
        <div className='centered'>
          <LoadingSpinner />
        </div>
      }>
        <Switch>
          {!isAuthenticated && <Route path="*"><Login onLogin={onLoginHandler} /></Route>}
          <Redirect exact from='/' to="/books"></Redirect>
          <Route path="/books" exact><AllBooks getBooks={getBooks} setBooks={setBooks} /></Route>
          <Route path="/books/:bookId"><BookDetails getBooks={getBooks} /></Route>
          <Route path="/new-book"  ><NewBook getBooks={getBooks} setBooks={setBooks}/></Route>
          <Route path="/delete"><DeleteBook getBooks={getBooks} setBooks={setBooks} /></Route>
          <Route path='/edit' exact><UpdateBook getBooks={getBooks} setBooks={setBooks} /></Route>
          <Route path='/edit/:editId'><EditBookDetails getBooks={getBooks} setBooks={setBooks} /></Route>
          <Route path="*"><PageNotFound /></Route>
        </Switch>
        </Suspense>
      </Layout>
  );
}

export default withRouter(Router);
// setIsAuthenticated={setIsAuthenticated}
/*   {!isAuthenticated && <Route path="*"><Login onLogin={() => {
  setIsAuthenticated(true)
  props.history.push("/books")
}}/></Route>}*/